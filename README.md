# Tuto Git

## Connexion au repository via SSH

* Générer sa paire de clés SSH: ssh-keygen -t rsa -b 4096 -C "email@example.com"
* Récupérer la clé publique générée, path par défaut "~\.ssh\id_rsa.pub"
* Ajouter la clé publique dans GitLab: Profile > Settings > SSH Keys

## Commande de base

* git init: initialisation d'un dépôt Git
* git clone "url": récupère toutes les données d'un projet existant
* git add "nom de fichier": ajoute un fichier au tracking de son dépôt
* git commit -m "commentaire": ajoute les fichiers tracker à la branche actuelle
* git checkout
* fetch: télécharge les sources et références depuis un autre dépôt
* pull: incorpore les changements depuis un dépôt distant à la branche actuelle
* push: met à jour les références du dépôt avec les références locales, envoyant les sources nécessaires à cette mise à jour

test